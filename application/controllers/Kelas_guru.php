<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas_guru extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('M_kelas_guru');
		$this->load->model('M_tahun');
		$this->load->model('M_kelas');
	}

	public function index()
	{
		if($this->session->userdata('nama')!=""){
			$taunaktif=$this->M_tahun->gettaunaktif();
			$data['view']='Master/kelas_guru';
			$data["thn_ajaran"]=$taunaktif;
			$data["siswa"]=$this->M_kelas_guru->getsiswa();
			$this->load->view("template/template",$data);
		}else{
			$this->session->set_flashdata('gagal', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Harus Login Terlebih Dahulu</div>");
			redirect(base_url());
		}
	}

	public function tambah_kelas_siswa(){
		if($this->session->userdata('nama')!=""){
			$taunaktif=$this->M_tahun->gettaunaktif();
			$data["siswa"]=$this->M_kelas_guru->getsiswatambah();
			$data['view']='Master/tambah_siswa_kelas';
			$data['kelas']=$this->M_kelas->getdata();
			$this->load->view("template/template",$data);
		}else{
			$this->session->set_flashdata('gagal', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Harus Login Terlebih Dahulu</div>");
			redirect(base_url());
		}

	}

	public function simpan_data_siswa(){
		$this->M_kelas_guru->simpan_kelas_siswa();
	}

	public function hapus_siswa_kelas($id_kelas_guru){
		$this->M_kelas_guru->hapus($id_kelas_guru);
	}
	
}