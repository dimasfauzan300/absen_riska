<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function index()
	{
		if($this->session->userdata('id')!=""){
			$data['view']='welcome_message';
			$this->load->view('template/template',$data);
		}else{
			redirect(base_url());
		}
	}
}
