<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_kelas');
	}

	public function index()
	{
		if($this->session->userdata('nama')!=""){
			$data['view']='Master/kelas';
			$data['data']=$this->M_kelas->getdata();
			$this->load->view("template/template",$data);
		}else{
			$this->session->set_flashdata('gagal', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Harus Login Terlebih Dahulu</div>");
			redirect(base_url());
		}
	}

	public function tambah_kelas(){
		$nama_kelas=$this->input->post('nama_kelas');
		$data = array('nama_kelas' => $nama_kelas );
		$this->M_kelas->insert_kelas($data);
		$this->session->set_flashdata('gagal', "<div class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-check'></i> Sukses!</h4> Data Berhasil Disimpan</div>");
		redirect(base_url("index.php/Kelas"));
	}

	public function edit_kelas(){
		$id_kelas=$this->input->post('id_kelas');
		$nama_kelas=$this->input->post('nama_kelas');
		$data = array('nama_kelas' => $nama_kelas );
		$this->M_kelas->edit($id_kelas,$data);
		$this->session->set_flashdata('gagal', "<div class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-check'></i> Sukses!</h4> Data Berhasil Disimpan</div>");
		redirect(base_url("index.php/Kelas"));
	}
}
