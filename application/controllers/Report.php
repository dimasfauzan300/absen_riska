<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('M_kelas_guru');
		// $this->load->model('M_siswa');
		$this->load->model('M_report');
		// $this->load->model('M_report');
		date_default_timezone_set("Asia/Jakarta");
		// $this->load->add_package_path( APPPATH . 'third_party/fpdf');
	}

	public function index()
	{
		if($this->session->userdata('nama')!=""){
			$data["view"]='Report/cetak_absen';
			$data['siswa']=$this->M_report->getdata();
			$data['tanggal']=date("d-m-Y");			
			$this->load->view("template/template",$data);
		}else{
			$this->session->set_flashdata('gagal', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Harus Login Terlebih Dahulu</div>");
			redirect(base_url());
		}
	}

	public function getabsentgl(){
		if($this->session->userdata('nama')!=""){
			$tgl=$this->input->post("tgl_absen");
			$data["view"]='Report/cetak_absen';
			$data["siswa"]=$this->M_report->getdata($tgl);
			$data['tanggal']=date("d-m-Y",strtotime($tgl));
			$data['tanggal2']=$this->input->post("tgl_absen");
			$this->load->view("template/template",$data);
		}else{
			$this->session->set_flashdata('gagal', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Harus Login Terlebih Dahulu</div>");
			redirect(base_url());
		}
	}

	public function print($tanggal){		
		$pdf = new fpdf();
		$data=$this->M_report->getdataprint($tanggal);
		// var_dump($data);
		// exit();
		$pdf->AddPage('P','A4',0);
		$pdf->SetFont('Times','B',14);
		$pdf->Cell(80);
		$pdf->Cell(30,10,'Data Absen Siswa SDN 1 Maracang',0,0,'C');
		$pdf->Ln();
		$pdf->SetFont('Times','',10);
		$pdf->Cell(10);
		$pdf->Cell(20,10,"Tanggal : ",0);
		$pdf->Cell(20,10,date("d-m-Y",strtotime($tanggal)),0);
		$pdf->Ln();
		$nis="";
		$no=1;
		$pdf->SetFont('Times','B',10);
		$pdf->Cell(10);
		$pdf->Ln();
		$pdf->Cell(10);
		$pdf->Cell(40,7,"No",1,0,'C');
		$pdf->Cell(35,7,"NIS",1,0,'C');
		$pdf->Cell(40,7,"Nama",1,0,'C');
		$pdf->Cell(45,7,"Absen",1,0,'C');
		$pdf->Ln();
		$pdf->SetFont('Times','',10);		
		foreach ($data as $absen) {
			$pdf->Cell(10);
			$pdf->Cell(40,7,$no,1,0,'C');$no++;
			$pdf->Cell(35,7,$absen['nis'],1,0,'C');
			$pdf->Cell(40,7,$absen['nama_lengkap'],1,0,'C');
			$pdf->Cell(45,7,$absen['absen'],1,0,'C');
			$pdf->Ln();			
		}				
		$pdf->Ln();
		$pdf->Ln();
		$no=1;
		$pdf->SetFont('Times','',10);
		$pdf->Cell(10);
        $nama="absen_".date("d_m_Y");
        $pdf->Output( "$nama.pdf",'I' );
	}
}
