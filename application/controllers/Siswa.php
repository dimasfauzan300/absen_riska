<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('M_siswa');
	}

	public function index()
	{
		if($this->session->userdata('nama')!=""){
			$data['view']='Master/siswa';
			$data['data']=$this->M_siswa->getdata();
			$this->load->view("template/template",$data);
		}else{
			$this->session->set_flashdata('gagal', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Harus Login Terlebih Dahulu</div>");
			redirect(base_url());
		}
	}

	public function tambah_siswa(){
		$nis=$this->input->post('nis');
		$nama_lengkap=$this->input->post('nama_lengkap');
		$tempat_lahir=$this->input->post('tempat_lahir');
		$tanggal_lahir=$this->input->post('tanggal_lahir');
		$jenis_kelamin=$this->input->post('jenis_kelamin');
		$alamat=$this->input->post('alamat');
		$agama=$this->input->post('agama');
		$nama_ortu=$this->input->post('nama_ortu');
		$no_telp_ortu=$this->input->post('no_telp_ortu');
		$stat_siswa=$this->input->post('stat_siswa');
		$data = array(
			'nis' => $nis,
			'nama_lengkap' => $nama_lengkap,
			'tempat_lahir' => $tempat_lahir,
			'tanggal_lahir' => $tanggal_lahir,
			'jenis_kelamin' => $jenis_kelamin,
			'alamat' => $alamat,
			'agama' => $agama,
			'nama_ortu' => $nama_ortu,
			'no_telp_ortu' => $no_telp_ortu,						
		);
		$this->M_siswa->insert_siswa($data);
		redirect(base_url("index.php/Siswa"));
	}

	public function edit_siswa(){
		$nis=$this->input->post('nis');
		$nama_lengkap=$this->input->post('nama_lengkap');
		$tempat_lahir=$this->input->post('tempat_lahir');
		$tanggal_lahir=$this->input->post('tanggal_lahir');
		$jenis_kelamin=$this->input->post('jenis_kelamin');
		$alamat=$this->input->post('alamat');
		$agama=$this->input->post('agama');
		$nama_ortu=$this->input->post('nama_ortu');
		$no_telp_ortu=$this->input->post('no_telp_ortu');
		$stat_siswa=$this->input->post('stat_siswa');
		$data = array(
			'nis' => $nis,
			'nama_lengkap' => $nama_lengkap,
			'tempat_lahir' => $tempat_lahir,
			'tanggal_lahir' => $tanggal_lahir,
			'jenis_kelamin' => $jenis_kelamin,
			'alamat' => $alamat,
			'agama' => $agama,
			'nama_ortu' => $nama_ortu,
			'no_telp_ortu' => $no_telp_ortu
			);
		$this->M_siswa->edit($nis,$data);
		redirect(base_url("index.php/Siswa"));
	}
}
