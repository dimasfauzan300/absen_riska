<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tahun extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('M_tahun');
	}

	public function index()
	{
		if($this->session->userdata('nama')!=""){
			$data['view']='Master/tahun';
			$data['data']=$this->M_tahun->getdata();
			$this->load->view("template/template",$data);
		}else{
			$this->session->set_flashdata('gagal', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Harus Login Terlebih Dahulu</div>");
			redirect(base_url());
		}
	}

	public function tambah_semester(){
		$cek_aktif=$this->M_tahun->cek_aktif_semester();
		if($cek_aktif>0){
			$this->session->set_flashdata('gagal', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Nonaktifkan Semester Yang Aktif Terlebih Dahulu</div>");
			redirect(base_url("index.php/Tahun"));
		}else{
			$id_semester=$this->input->post("id_semester");
			$nama_semester=$this->input->post("nama_semester");
			$tahun_ajaran=$this->input->post("thn_ajaran");
			$data = array('id_semester' => $id_semester, 
				'nama_semester' => $nama_semester,
				'periode_aktif ' => 1,
				'thn_ajaran' => $tahun_ajaran
			);
			$this->M_tahun->simpan_semester($data);
			$this->session->set_flashdata('gagal', "<div class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-check'></i> Sukses!</h4> Data Berhasil Disimpan</div>");
			redirect(base_url("index.php/Tahun"));
		}
	}

	public function edit_semester(){
		$id_semester=$this->input->post("id_semester");
		$nama_semester=$this->input->post("nama_semester");
		$tahun_ajaran=$this->input->post("thn_ajaran");
		$periode_aktif=$this->input->post("periode_aktif");
		$data = array('nama_semester' => $nama_semester,
			'periode_aktif ' => $periode_aktif,
			'thn_ajaran' => $tahun_ajaran
		);
		//var_dump($data);exit;
		$this->M_tahun->edit_semester($id_semester,$data);
		$this->session->set_flashdata('gagal', "<div class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-check'></i> Sukses!</h4> Data Berhasil Disimpan</div>");
		redirect(base_url("index.php/Tahun"));
	}

	public function gettaunaktif(){
		$semester="";
		$this->db->select("id_smester");
		$this->db->from("tahun_ajaran");
		$this->db->where("periode_aktif",1);
		$res=$this->db->get();
		foreach ($res->result() as $data) {
			$semester=$data->id_semester;
		}
		return $semester;
	}	
}