<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absen extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('M_kelas_guru');
		// $this->load->model('M_siswa');
		$this->load->model('M_absen');
		date_default_timezone_set("Asia/Jakarta");
		// $this->load->add_package_path( APPPATH . 'third_party/fpdf');
	}

	public function index()
	{
		if($this->session->userdata('nama')!=""){
			$semester=getSemesterAktif();
			$data["view"]='Absesnsi/absen';
			$data['siswa']=$this->M_kelas_guru->getsiswa($semester);
			$this->load->view("template/template",$data);
		}else{
			$this->session->set_flashdata('gagal', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Harus Login Terlebih Dahulu</div>");
			redirect(base_url());
		}
	}

	public function simpan(){
		$this->M_absen->insert_absen();				
	}

	public function lihat_absen(){
		if($this->session->userdata('nama')!=""){
			$data['view']='Absesnsi/det_absen';
			$data['absen']=$this->M_absen->getdata();
			$this->load->view("template/template",$data);
		}else{
			$this->session->set_flashdata('gagal', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Harus Login Terlebih Dahulu</div>");
			redirect(base_url());
		}
	}

	public function edit_absen(){
		$id_absen=$this->input->post("id_absen");	
		$absen=$this->input->post("absen");
		if($absen==""){
			redirect(base_url("index.php/Absen/lihat_absen"));
		}else{
			$data = array('absen' => $absen );
			$this->M_absen->edit($id_absen,$data);
			$this->session->set_flashdata('gagal', "<div class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-check'></i> Sukses!</h4> Data Berhasil Disimpan</div>");
			redirect(base_url("index.php/Absen/lihat_absen"));
		}		
	}

	public function print(){		
		$pdf = new fpdf();
		$data=$this->M_absen->get_data_print();
		$pdf->AddPage('P','A4',0);
		$pdf->SetFont('Arial','',14);
		$nis="";
		foreach ($data->result() as $absen) {
			$nis=$absen->nis;
			if($absen->nis!=$nis){
				$pdf->Ln();
				$pdf->Cell(40,6,$absen->nis,1);
				$pdf->Cell(40,6,$absen->nama_lengkap,1);				
			}
			$pdf->Cell(40,6,$absen->nis,1);
			$pdf->Cell(40,6,$absen->nama_lengkap,1);
			$pdf->Cell(40,6,$absen->absen,1);
			
		}
        $nama="absen_".date("d_m_Y");
        $pdf->Output( "$nama.pdf" , 'I' );
	}
}
