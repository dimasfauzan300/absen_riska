<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Auth');
		$this->load->model('M_akun');
	}

	public function index()
	{
		$data['view']="Master/akun";
		$data['guru']=$this->M_akun->getdata();
		$this->load->view('template/template',$data);
	}


	public function edit_akun(){
		$id_guru=$this->input->post('id_guru');
		$nip=$this->input->post('nip');
		$nama_lengkap=$this->input->post('nama_lengkap');
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$data = array('id_guru' => $id_guru,
			'nip' => $nip, 
			'nama_lengkap'	 => $nama_lengkap, 
			'username'	 => $username,
			'password' => $password
		);
		$this->M_akun->simpan_akun($id_guru,$data);
		$sesi = array(
				'id_guru' =>$id_guru,
				'id'    => $nip,
				'nama'  => $nama_lengkap				
		);			
		$this->session->set_userdata($sesi);
		$this->session->set_flashdata('gagal', "<div class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-check'></i> Sukses!</h4> Data Berhasil Disimpan</div>");
		redirect(base_url("index.php/Akun"));
	}

	public function getpass($nip){
		// 	$nip=$this->input->post('nip');
		$res=$this->M_akun->getpassword($nip);
		echo json_encode($res);
	}
	
}
