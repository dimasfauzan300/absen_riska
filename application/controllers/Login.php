<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Auth');
	}

	public function index()
	{
		$this->load->view('login');
	}

	public function login(){
		$where    = array(
			'username' => $this->input->post('username'),
			'password' => $this->input->post('pass')
		);
		$cek = $this->Auth->cek_login($where);

		if($cek->num_rows() > 0) {
			foreach($cek->result() as $row) {
				$nip    = $row->nip;
				$nama  = $row->nama_lengkap;
				$id=$row->id_guru;				
			}

			$sesi = array(
				'id_guru' =>$id,
				'id'    => $nip,
				'nama'  => $nama,				
			);			
			
			$this->session->set_userdata($sesi);
			redirect(base_url("index.php/Welcome"));
		} else {
			$this->session->set_flashdata('gagal', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Username/Passwords Salah</div>");
			redirect(base_url());
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
