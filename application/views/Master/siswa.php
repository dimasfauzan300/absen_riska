<section class="content-header">  
  <div class="row">
  <div class="col-xs-12"><?= $this->session->flashdata('gagal'); ?></div>
</div>

    <!-- Main content -->
    <!-- /.content -->

    <div class="row">
    <div class="col-xs-12">
       <div class="box">
            <div class="box-header">
              <h3 class="box-title">Pilihan</h3>
            </div>
            <!-- /.box-header -->
           <div class="box-body">

             <div class="row">               
              <div class="col-md-3"><a href="#" role="menuitem" tabindex="-1" class="btn btn-info tambah_btn" data-toggle="modal" data-target="#TambahKelas"><i class="fa fa-plus-square"></i> Tambah Data Siswa</a></div>
             </div>
           </div>
            <!-- /.box-body -->
          </div>
    </div>
  </div>
</section>

<section class="content">
  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Siswa SD Negeri 1 Maracang</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="text-center">No</th>                  
                  <th class="text-center">NIS</th>
                  <th class="text-center">Nama Lengkap</th>
                  <th class="text-center">Tempat Lahir</th>
                  <th class="text-center">Tanggal Lahir</th>
                  <th class="text-center">Jenis Kelamin</th>
                  <th class="text-center">Alamat</th>
                  <th class="text-center">Agama</th>
                  <th class="text-center">Nama Orang Tua</th>
                  <th class="text-center">No Telepon Orang Tua</th>                  
                  <th class="text-center">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php  foreach  ($data as $siswa) { $no=1; ?>
                    <tr>
                      <td class="text-center"><?php echo $no;?></td>                      
                      <td class="text-center"><?php echo $siswa['nis'];?></td>
                      <td class="text-center"><?php echo $siswa['nama_lengkap'];?></td>
                      <td class="text-center"><?php echo $siswa['tempat_lahir'];?></td>
                      <td class="text-center"><?php echo date("d-m-Y",strtotime($siswa['tanggal_lahir']));?></td>
                      <td class="text-center"><?php echo $siswa['jenis_kelamin'];?></td>
                      <td class="text-center"><?php echo $siswa['alamat'];?></td>
                      <td class="text-center"><?php echo $siswa['agama'];?></td>
                      <td class="text-center"><?php echo $siswa['nama_ortu'];?></td>
                      <td class="text-center"><?php echo $siswa['no_telp_ortu'];?></td>                      
                      <td class="text-center"><a href="#" role="menuitem" tabindex="-1" class="btn btn-info edit_btn" data-toggle="modal" data-target="#EditSiswa" data-nis="<?php echo $siswa['nis'];?>" data-nama="<?php echo $siswa['nama_lengkap'];?>" data-tl="<?php echo $siswa['tempat_lahir'];?>" data-lhr="<?php echo $siswa['tanggal_lahir'];?>" data-jenkel="<?php echo $siswa['jenis_kelamin'];?>" data-alamat="<?php echo $siswa['alamat'];?>" data-agama="<?php echo $siswa['agama'];?>" data-ortu="<?php echo $siswa['nama_ortu'];?>" data-telp="<?php echo $siswa['no_telp_ortu'];?>" data-stat="<?php echo $siswa['stat_siswa'];?>" id="btn_edit"><i class="fa fa-edit"> Edit</a></td>
                    </tr>
                  <?php $no++; }?>                  
                </tbody>
                <tfoot>
                <tr>
                  <th class="text-center">No</th>                  
                  <th class="text-center">NIS</th>
                  <th class="text-center">Nama Lengkap</th>
                  <th class="text-center">Tempat Lahir</th>
                  <th class="text-center">Tanggal Lahir</th>
                  <th class="text-center">Jenis Kelamin</th>
                  <th class="text-center">Alamat</th>
                  <th class="text-center">Agama</th>
                  <th class="text-center">Nama Orang Tua</th>
                  <th class="text-center">No Telepon Orang Tua</th>
                  <th class="text-center">Status Siswa</th>
                  <th class="text-center">Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
</section>

<div class="modal fade" id="TambahKelas" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog" style="width:80%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Tambah Siswa</h4>
        </div>
        <div class="modal-body">
          
          <form role="form" action="<?php echo base_url();?>index.php/Siswa/tambah_siswa" method="post">
              <div class="row">
                <div class="col-md-6">
                    <label>NIS</label>
                    <input type="text" name="nis" class="form-control">
                </div>
              
                <div class="col-md-6 ">
                    <label>Nama Lengkap</label>
                    <input type="text" name="nama_lengkap" class="form-control">
                </div>
            
                <div class="col-md-6">
                <label>Tempat Lahir</label>
                <input type="text" name="tempat_lahir" class="form-control">
              </div>

              <div class="col-md-6">
                <label>Tanggal Lahir</label>
                <input type="date" name="tanggal_lahir" class="form-control">
              </div>
              <div class="col-md-6">
                <label>Jenis Kelamin</label>
                <select name="jenis_kelamin" class="form-control">
                <option value="">Pilih</option>
                <option value="Laki-laki">Laki-Laki</option>
                <option value="Perempuan">Perempuan</option>
                </select>
              </div>
              <div class="col-md-6">
                <label>Alamat</label>
                <input type="text" name="alamat" class="form-control">
              </div>
              <div class="col-md-6">
                <label>Agama</label>
                <select name="agama" class="form-control">
                                <option value="">Pilih</option>
                                <option value="Islam">Islam</option>
                                <option value="Protestan">Protestan</option>
                                <option value="Katolik">Katolik</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Budha">Budha</option>
                                <option value="Khonghucu">Khonghucu</option>
                              </select>
              </div>
              <div class="col-md-6">
                <label>Nama Orang Tua</label>
                <input type="text" name="nama_ortu" class="form-control">
              </div>
              <div class="col-md-6">
                <label>No Telepon Orang Tua</label>
                <input type="text" name="no_telp_ortu" class="form-control">
              </div>              
            </div>
            <br>
              <div class="row">
                <div class="col-xs-12">
                  <div class="form-group">              
                <button class="btn btn-info" id="simpan-thn-ajar">Simpan</button>
              </div>    
                </div>
              </div>
              
            </form>
          </div>
        </div>
      </div>
    </div>


<div class="modal fade" id="EditSiswa" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog" style="width:80%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Siswa</h4>
        </div>
        <div class="modal-body">
          
          <form role="form" action="<?php echo base_url();?>index.php/Siswa/edit_siswa" method="post">
              <div class="row">
                <div class="col-md-6">
                    <label>NIS</label>
                    <input type="text" name="nis" class="form-control" id="nis">
                </div>
              
                <div class="col-md-6 ">
                    <label>Nama Lengkap</label>
                    <input type="text" name="nama_lengkap" class="form-control" id="nama_lengkap">
                </div>
            
                <div class="col-md-6">
                <label>Tempat Lahir</label>
                <input type="text" name="tempat_lahir" class="form-control" id="tempat_lahir">
              </div>

              <div class="col-md-6">
                <label>Tanggal Lahir</label>
                <input type="date" name="tanggal_lahir" class="form-control" id="tanggal_lahir">
              </div>
              <div class="col-md-6">
                <label>Jenis Kelamin</label>
                <select name="jenis_kelamin" class="form-control">
                <option value="Laki-laki">Laki-Laki</option>
                <option value="Perempuan">Perempuan</option>
                </select>
              </div>
              <div class="col-md-6">
                <label>Alamat</label>
                <textarea name="alamat" class="form-control" id="alamat"></textarea>
              </div>
              <div class="col-md-6">
                <label>Agama</label>
                <select name="agama" class="form-control">
                                <option value="Islam">Islam</option>
                                <option value="Protestan">Protestan</option>
                                <option value="Katolik">Katolik</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Budha">Budha</option>
                                <option value="Khonghucu">Khonghucu</option>
                              </select>
              </div>
              <div class="col-md-6">
                <label>Nama Orang Tua</label>
                <input type="text" name="nama_ortu" class="form-control" id="nama_ortu">
              </div>
              <div class="col-md-6">
                <label>No Telepon Orang Tua</label>
                <input type="text" name="no_telp_ortu" class="form-control" id="no_telp_ortu">
              </div>             
            </div>
            <br>
              <div class="row">
                <div class="col-xs-12">
                  <div class="form-group">              
                <button class="btn btn-info" id="simpan-thn-ajar">Simpan</button>
              </div>    
                </div>
              </div>
              
            </form>
          </div>
        </div>
      </div>
    </div>

<script src="<?php echo base_url();?>asset/temp/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script>
  $(function () {
    $('#example1').DataTable();
  });

  $(".edit_btn").on('click',function(){ //fungsi ketika button edit di click dasarana mun class awalna pke '.' lamun id make "#" $("awal+namaattr").on('event nu jd trigger click mun di klik dst',function(){} didie bisa fungsi terpisah bisa make lamda fungsi);
      //menangkap data dari atttribut dari button edit  
      var nis=$(this).attr("data-nis");
      var nama_lengkap=$(this).attr("data-nama");
      var tempat_lahir=$(this).attr("data-tl");
      var tanggal_lahir=$(this).attr("data-lhr");
      var jenis_kelamin=$(this).attr("data-jenkel");
      var alamat=$(this).attr("data-alamat");
      var agama=$(this).attr("data-agama");
      var nama_ortu=$(this).attr("data-ortu");
      var no_telp_ortu=$(this).attr("data-telp");
      var stat_siswa=$(this).attr("data-stat");
      //set data yang ditangkap ke modal edit
      $("#nis").attr('value',nis);
      $("#nama_lengkap").attr('value',nama_lengkap);
      $("#tempat_lahir").attr('value',tempat_lahir);
      $("#tanggal_lahir").attr('value',tanggal_lahir);
      $("#jenis_kelamin").attr('value',jenis_kelamin);
      $("#alamat").text(alamat);
      $("#agama").attr('value',agama);
      $("#nama_ortu").attr('value',nama_ortu);
      $("#no_telp_ortu").attr('value',no_telp_ortu);
      $("#stat_siswa").attr('value',stat_siswa);
    });
</script>