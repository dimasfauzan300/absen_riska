<section class="content-header">  
  <div class="row">
  <div class="col-xs-12"><?= $this->session->flashdata('gagal'); ?></div>
</div>
<div class="row">
  <div class="col-xs-12"></div>
</div>
</section>

    <!-- Main content -->
    <!-- /.content -->
      <form method="post" action="<?php echo base_url();?>index.php/Kelas_guru/simpan_data_siswa">
<section class="content">
  <div class="row">
    <div class="col-xs-12"><?= $this->session->flashdata('gagal1'); ?></div>
  </div>
   <div class="row">
    <div class="col-xs-12">
       <div class="box">
            <div class="box-header">
              <h3 class="box-title">Pilihan</h3>
            </div>
            <!-- /.box-header -->
           <div class="box-body">
            <form method="post" action="<?php echo base_url();?>index.php/Kelas_guru/simpan_data_siswa">
            <table>
                  <tr>
                    <td class="text-center"> 
                      <select class="form-control" name="kelas">
                        <option>Pilih Kelas</option>
                    <?php foreach ($kelas as $data_kelas) { ?>
                      <option value="<?php echo $data_kelas['id_kelas'];?>"><?php echo $data_kelas['nama_kelas'];?></option>
                    <?php } ?>
                  </select>
                </td>
                  <td>&nbsp</td>
                  <td class="text-center"><button class="btn btn-success"><i class="fa fa-floppy-o"></i> Simpan</button></td>
                  <td>&nbsp</td>
                  <td class="text-center"> <a class="btn btn-warning" href="<?php echo base_url();?>index.php/Kelas_guru/"><i class="fa fa-reply"></i> Cancel</a></td>
                  </tr>
                </table>
            <!-- /.box-body -->
          </div>
    </div>
  </div>
</div>
  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Siswa Kelas SD Negeri 1 Maracang</h3>              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                                       
              <div class="row">
                <div class="col-xs-12">
                  <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="text-center"></th>                  
                  <th class="text-center">NIS</th>
                  <th class="text-center">Nama</th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach ($siswa as $data) {  ?>
                    <tr>
                      <?php $cek=ceksiswa($data['nis']); if($cek==1){ ?>
                          <td class="text-center"><i class="fa fa-check"></i></td>
                      <?php  }else{  ?>
                        <td class="text-center"><input type="checkbox" class="minimal" name="siswa[]" value="<?php echo $data['nis'];?>"></td>
                      <?php } ?>                      
                      <td class="text-center"><?php echo $data['nis'];?></td>
                      <td class="text-center"><?php echo $data['nama_lengkap'];?></td>
                    </tr>
                  <?php }?>
                </tbody>
                <tfoot>
                <tr>
                 <th class="text-center"></th>                  
                  <th class="text-center">NIS</th>
                  <th class="text-center">Nama</th>
                </tr>
                </tfoot>
              </table>
                </div>
              </div>                            
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
       
      </div>

</section>
 </form>

<script src="<?php echo base_url();?>asset/temp/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable();
  });
   $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })
</script>