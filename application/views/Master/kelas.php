<section class="content-header">  
  <div class="row">
  <div class="col-xs-12"><?= $this->session->flashdata('gagal'); ?></div>
</div>

 <div class="row">
    <div class="col-xs-12">
       <div class="box">
            <div class="box-header">
              <h3 class="box-title">Pilihan</h3>
            </div>
            <!-- /.box-header -->
           <div class="box-body">

             <div class="row">               
              <div class="col-md-3"><a href="#" role="menuitem" tabindex="-1" class="btn btn-info tambah_btn" data-toggle="modal" data-target="#TambahKelas"><i class="fa fa-plus-square"></i> Tambah Data Kelas</a></div>
             </div>
           </div>
            <!-- /.box-body -->
          </div>
    </div>
  </div>
</section>

    <!-- Main content -->
    <!-- /.content -->
<section class="content">
  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Kelas SD Negeri 1 Maracang</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="text-center">No</th>                  
                  <th class="text-center">Nama Kelas</th>
                  <th class="text-center">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach ($data as $kelas) {  ?>
                    <tr>
                      <td class="text-center"><?php echo $no;$no++;?></td>                      
                      <td class="text-center"><?php echo $kelas['nama_kelas'];?></td>
                      <td class="text-center"><a href="#" role="menuitem" tabindex="-1" class="btn btn-info edit_btn" data-toggle="modal" data-target="#EditKelas" data-id="<?php echo $kelas['id_kelas'];?>" data-nama="<?php echo $kelas['nama_kelas'];?>" id="btn_edit"><i class="fa fa-edit"> Edit</a></td>
                    </tr>
                  <?php }?>
                </tbody>
                <tfoot>
                <tr>
                  <th class="text-center">No</th>
                  <th class="text-center">Nama Kelas</th>
                  <th class="text-center">Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
</section>

<div class="modal fade" id="TambahKelas" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Tambah Kelas</h4>
        </div>
        <div class="modal-body">
          <form role="form" action="<?php echo base_url();?>index.php/Kelas/tambah_kelas" method="post">
              <div class="form-group">
                <label>Nama Kelas</label>
                <input type="text" name="nama_kelas" class="form-control" required="true">
              </div>
              <br>
              <div class="row">
                <div class="col-md-3 pull-right">
                  <button class="btn btn-success" id="simpan-thn-ajar"><i class="fa fa-floppy-o"></i> Simpan</button>
                </div>                
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>


<div class="modal fade" id="EditKelas" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Kelas</h4>
        </div>
        <div class="modal-body">
          <form role="form" method="post" action="<?php echo base_url();?>index.php/Kelas/edit_kelas">
            <input type="text" id="id_kelas" value="" hidden="true" name="id_kelas">
              <div class="form-group">
                <label>Nama Kelas</label>
                <input type="text" name="nama_kelas" class="form-control" value="" id="nama_kelas" required="true">
              </div>
              <br>
              <div class="row">
                <div class="col-md-3 pull-right">
                  <button class="btn btn-success" id="simpan-thn-ajar"><i class="fa fa-floppy-o"></i> Simpan</button>
                </div>                
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

<script src="<?php echo base_url();?>asset/temp/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable();
  });

  $(".edit_btn").on('click',function(){ //fungsi ketika button edit di click dasarana mun class awalna pke '.' lamun id make "#" $("awal+namaattr").on('event nu jd trigger click mun di klik dst',function(){} didie bisa fungsi terpisah bisa make lamda fungsi);
      //menangkap data dari atttribut dari button edit  
      var id_kelas=$(this).attr("data-id");
      var nama_kelas=$(this).attr("data-nama");
      //set data yang ditangkap ke modal edit
      $("#id_kelas").attr('value',id_kelas);
      $("#nama_kelas").attr('value',nama_kelas);
    });
</script>