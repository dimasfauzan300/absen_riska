<section class="content-header">
<div class="row">
  <div class="col-xs-12"><?= $this->session->flashdata('gagal'); ?></div>
</div>

<div class="row">
    <div class="col-xs-12">
       <div class="box">
            <div class="box-header">
              <h3 class="box-title">Pilihan</h3>
            </div>
            <!-- /.box-header -->
           <div class="box-body">

             <div class="row">               
              <div class="col-md-3"><a href="#" role="menuitem" tabindex="-1" class="btn btn-info tambah_btn" data-toggle="modal" data-target="#TambahKelas"><i class="fa fa-plus-square"></i>  Tambah Data Tahun Ajaran</a></div>
             </div>
           </div>
            <!-- /.box-body -->
          </div>
    </div>
  </div>
</section>

    <!-- Main content -->
    <!-- /.content -->
<section class="content">
  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Tahun Ajaran</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="text-center">ID Semester</th>                  
                  <th class="text-center">Semester</th>
                  <th class="text-center">Status</th>
                  <th class="text-center">Tahun Ajaran</th>
                  <th class="text-center">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php foreach ($data as $tahun) { ?>
                    <tr>
                      <td class="text-center"><?php echo $tahun['id_semester']?></td>                      
                      <td class="text-center"><?php echo $tahun['nama_semester'];?></td>
                      <?php if ($tahun['periode_aktif']==1) { ?>
                        <td class="text-center"><span class="label label-success">Aktif</span></td>
                      <?php }else{ ?>
                        <td class="text-center"><span class="label label-danger">Nonaktif</span></td>
                      <?php } ?>
                      <td class="text-center"><?php echo $tahun['thn_ajaran'];?></td>
                      <td class="text-center"><a href="#" role="menuitem" tabindex="-1" class="btn btn-info edit_btn" data-toggle="modal" data-target="#EditKelas" data-id="<?php echo $tahun['id_semester'];?>" data-sem="<?php echo $tahun['nama_semester'];?>" aktif="<?php echo $tahun['periode_aktif'];?>" tahun="<?php echo $tahun['thn_ajaran'];?>" id="btn_edit"><i class="fa fa-edit"> Edit</a></td>
                    </tr>
                  <?php }?>
                </tbody>
                <tfoot>
                <tr>
                  <th class="text-center">ID Semester</th>                  
                  <th class="text-center">Semester</th>
                  <th class="text-center">Status</th>
                  <th class="text-center">Tahun Ajaran</th>
                  <th class="text-center">Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
</section>

<div class="modal fade" id="TambahKelas" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Tambah Data Semester</h4>
        </div>
        <div class="modal-body">
          <form role="form" action="<?php echo base_url();?>index.php/Tahun/tambah_semester" method="post">
              <div class="form-group">
                <label>Tahun Ajaran</label>
                <input type="text" name="thn_ajaran" class="form-control" required="true">
              </div>
              <div class="form-group">
                <label>ID Semester</label>
                <input type="text" name="id_semester" class="form-control" required="true">
              </div>              
              <div class="form-group">
                <label>Pilih Semester</label>
                <select class="form-control" name="nama_semester">
                  <option value="Ganjil">Ganjil</option>
                  <option value="Genap">Genap</option>
                </select>
              </div>
              <br>
              <div class="row">
                <div class="col-md-3 pull-right">
                  <button class="btn btn-success" id="simpan-thn-ajar"><i class="fa fa-floppy-o"></i> Simpan</button>
                </div>                
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>


<div class="modal fade" id="EditKelas" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Data Semester</h4>
        </div>
        <div class="modal-body">
          <form role="form" action="<?php echo base_url();?>index.php/Tahun/edit_semester" method="post">
             <div class="form-group">
                <label>Tahun Ajaran</label>
                <input type="text" name="thn_ajaran" class="form-control" id="thn_ajaran" required="true">
              </div>
              <div class="form-group">
                <label>ID Semester</label>
                <input type="text" name="id_semester" class="form-control" id="id_semester" required="true">
              </div>         
              <div class="form-group">
                <label>Pilih Semester</label>
                <select class="form-control" name="nama_semester">
                  <option value="Ganjil">Ganjil</option>
                  <option value="Genap">Genap</option>
                </select>
              </div>
              <div class="form-group">
                <label>Status Semester</label>
                <select name="periode_aktif" class="form-control">
                  <option value="1">Aktif</option>
                  <option value="0">Nonaktif</option>
                </select>
              </div>
              <br>
              <div class="row">
                <div class="col-md-3 pull-right">
                  <button class="btn btn-success" id="simpan-thn-ajar"><i class="fa fa-floppy-o"></i> Simpan</button>
                </div>                
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

<script src="<?php echo base_url();?>asset/temp/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable();
  });

  $(".edit_btn").on('click',function(){ //fungsi ketika button edit di click dasarana mun class awalna pke '.' lamun id make "#" $("awal+namaattr").on('event nu jd trigger click mun di klik dst',function(){} didie bisa fungsi terpisah bisa make lamda fungsi);
      //menangkap data dari atttribut dari button edit  
      var id_semester=$(this).attr("data-id");
      var nama_semester=$(this).attr("data-sem");
      var thn_ajaran=$(this).attr("tahun");
      //set data yang ditangkap ke modal edit
      $("#id_semester").attr('value',id_semester);
      $("#thn_ajaran").attr('value',thn_ajaran);
    });
</script>