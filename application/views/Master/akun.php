<section class="content-header">  
  <div class="row">
  <div class="col-xs-12"><?= $this->session->flashdata('gagal'); ?></div>
</div>
</section>

    <!-- Main content -->
    <!-- /.content -->
<section class="content">
  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <small class="text-danger"><i>*Mohon Untuk Perbarui Akun Anda</i></small>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form method="post" action="<?php echo base_url();?>index.php/Akun/edit_akun">
              <?php foreach ($guru as $data) { ?>
                <input type="hidden" name="id_guru" value="<?php echo $data['id_guru'];?>">
                <div class="form-group">
                  <label>NIP</label>
                  <input type="text" name="nip" value="<?php echo $data['nip'];?>" class="form-control" required>
                </div>
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" name="nama_lengkap" value="<?php echo $data['nama_lengkap'];?>" class="form-control" required>
                </div>
                <div class="form-group">
                  <label>Username</label>
                  <input type="text" name="username" value="<?php echo $data['username'];?>" class="form-control" required>
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" name="password" value="<?php echo $data['password'];?>" class="form-control" required>
                </div>                
              <?php } ?>
              <div class="pull-right">
                <button class="btn btn-success"><i class="fa fa-floppy-o"></i> Simpan</button>
              </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
</section>

<div class="modal fade" id="TambahKelas" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Tambah Kelas</h4>
        </div>
        <div class="modal-body">
          <form role="form" action="<?php echo base_url();?>index.php/Kelas/tambah_kelas" method="post">
              <div class="form-group">
                <label>Nama Kelas</label>
                <input type="text" name="nama_kelas" class="form-control" required="true">
              </div>
              <br>
              <div class="row">
                <div class="col-md-3 pull-right">
                  <button class="btn btn-success" id="simpan-thn-ajar"><i class="fa fa-floppy-o"></i> Simpan</button>
                </div>                
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>


<script src="<?php echo base_url();?>asset/temp/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable();
  });

  $("#btn_edit").on('click',function(){ //fungsi ketika button edit di click dasarana mun class awalna pke '.' lamun id make "#" $("awal+namaattr").on('event nu jd trigger click mun di klik dst',function(){} didie bisa fungsi terpisah bisa make lamda fungsi);
      //menangkap data dari atttribut dari button edit  
      var id_kelas=$(this).attr("data-id");
      var nama_kelas=$(this).attr("data-nama");
      //set data yang ditangkap ke modal edit
      $("#id_kelas").attr('value',id_kelas);
      $("#nama_kelas").attr('value',nama_kelas);
    });
</script>