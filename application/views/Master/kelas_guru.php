<section class="content-header">
  <div class="row">
  <div class="col-xs-12"><?= $this->session->flashdata('gagal'); ?></div>
</div>

  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          
        </div>
        <div class="box-body">          
          <div class="row">
            <div class="col-xs-12" style="padding-left: 30px;">
              <table>
                <tr height="5%">
                  <td>Nama </td>
                  <td width="10%" class="text-center">:</td>
                  <td><?php echo $this->session->userdata('nama');?></td>
                </tr>
                <tr>
                  <td>NIP</td>
                  <td width="10%" class="text-center">:</td>
                  <td><?php echo $this->session->userdata('id');?></td>
                </tr>
                <tr>
                  <td>Tahun Ajaran</td>
                  <td width="10%" class="text-center">:</td>
                  <td><?php 
                  // $str=substr($thn_ajaran, 4,5);
                  // echo $str;
                    if(substr($thn_ajaran, 4,5)=="1"){
                     
                        echo $thn_ajaran." (Ganjil)";
                    }else{
                      echo $thn_ajaran." (Genap)";
                    };?>
                      
                    </td>
                </tr>
              </table>
            </div>
          </div>          
        </div>
      </div>
    </div>    
  </div>
</section>

    <!-- Main content -->
    <!-- /.content -->
<section class="content">
  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row pull-right">
                <div class="col-xs-12">
                  <div class="form-group">
                    <a href="<?php echo base_url();?>index.php/Kelas_guru/tambah_kelas_siswa" class="btn btn-info tambah_btn" style="margin-left: 20px;"><i class="fa fa-plus-square"></i> Tambah Data Siswa</a>
                  </div>
                </div>
              </div> 
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="text-center">NIS</th>
                  <th class="text-center">Nama Siswa</th>
                  <th class="text-center">Kelas</th>                  
                  <th class="text-center">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                    if($siswa==""){

                    }else{
                      foreach ($siswa as $data) { ?>
                        <tr>
                          <td class="text-center"><?php echo $data['nis'];?></td>
                          <td class="text-center"><?php echo $data['nama_lengkap'];?></td>
                          <td class="text-center"><?php echo $data['nama_kelas'];?></td>                          
                          <td class="text-center"><a href="<?php echo base_url();?>index.php/Kelas_guru/hapus_siswa_kelas/<?php echo $data['id_kelas_guru'];?>" class="btn btn-danger">Delete <i class="fa fa-trash"></i></a></td>
                        </tr>
                      <?php }
                    }
                  ?>
                </tbody>
                <tfoot>
                <tr>
                  <th class="text-center">NIS</th>
                  <th class="text-center">Nama Siswa</th>
                  <th class="text-center">Kelas</th>                  
                  <th class="text-center">Aksi</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
</section>

<script src="<?php echo base_url();?>asset/temp/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable();
  });

</script>