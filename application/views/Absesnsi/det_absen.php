<section class="content-header">  
  <div class="row">
  <div class="col-xs-12"><?= $this->session->flashdata('gagal'); ?></div>
</div>
</section>

    <!-- Main content -->
    <!-- /.content -->
<section class="content">
    <div class="row">
    <div class="col-xs-12">
       <div class="box">
            <div class="box-header">
              <h3 class="box-title">Pilihan</h3>
            </div>
            <!-- /.box-header -->
           <div class="box-body">
            <table>
              <tr style="width: 40px;">               
                <td class="text-center"><a class="btn btn-warning" href="<?php echo base_url();?>index.php/Absen/"> <i class="fa fa-reply"></i> Kembali</a></td>
                <td>&nbsp </td>
                <td class="text-center"><a href="<?php echo base_url();?>index.php/Absen/print" class="btn btn-default"><i class="fa fa-print"></i> Print</a></td>
              </tr>
            </table>
             
            <!-- /.box-body -->
          </div>
    </div>
  </div>
</div>
  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <br>
              <h6 class="box-title pull-right">Tanggal : <?php echo date("d M Y");?></h6>
              <br>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="text-center">No</th>                  
                  <th class="text-center">NIS</th>
                  <th class="text-center">Nama</th>
                  <th class="text-center">Absen</th>
                  <th class="text-center">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php  $no=1; foreach ($absen as $data) { ?>
                    <tr>
                      <td class="text-center"><?php echo $no;?></td>                      
                      <td class="text-center"><?php echo $data['nis'];?></td>
                      <td class="text-center"><?php echo $data['nama_lengkap'];?></td>
                      <td class="text-center"><?php echo $data['absen'];?></td>
                      <td class="text-center"><a href="#" class="btn btn-info" id="edit_absen" role="menuitem" tabindex="-1" data-toggle="modal" data-target="#EditAbsen" data-nis="<?php echo $data['nis'];?>" data-absen="<?php echo $data['absen'];?>" id_absen="<?php echo $data['id_absen'];?>" data-nama="<?php echo $data['nama_lengkap'];?>"><i class="fa fa-edit"></i> Edit</a></td>
                    </tr>
                  <?php $no++; }?>
                </tbody>
                         
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
</section>

<div class="modal fade" id="EditAbsen" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Absen</h4>
        </div>
        <div class="modal-body">
          <form role="form" method="post" action="<?php echo base_url();?>index.php/Absen/edit_absen">
            <input type="hidden" id="id_absen" value="" name="id_absen">
              <table class="table table-stripped" style="width: 40%">
                <tr>
                  <td>NIS</td>
                  <td>:</td>
                  <td id="nis"></td>
                </tr>        
                <tr>
                  <td>Nama</td>
                  <td>:</td>
                  <td id="nama_lengkap"></td>
                </tr>
              </table>
              <br>
              <div class="form-group">               
                <table class="table table-bordered">
                  <tr>
                  <th class="text-center">Hadir</th>
                  <th class="text-center">Sakit</th>
                  <th class="text-center">Izin</th>
                  <th class="text-center">Alfa</th>
                  </tr>
                  <tr>                    
                      <td class="text-center"><input type="radio" name="absen" class="flat-red" value="H" id="hadir"></td>
                      <td class="text-center"><input type="radio" name="absen" class="flat-red" value="S" id="sakit"></td>
                      <td class="text-center"><input type="radio" name="absen" class="flat-red" value="I" id="izin"></td>
                      <td class="text-center"><input type="radio" name="absen" class="flat-red" value="A" id="alfa"></td>
                  </tr>
                </table>
              </div>
              <br>
              <div class="row">
                <div class="col-md-3 pull-right">
                  <button class="btn btn-success" id="simpan-absen"><i class="fa fa-floppy-o"></i> Simpan</button>
                </div>                
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

<script src="<?php echo base_url();?>asset/temp/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>asset/temp/plugins/iCheck/icheck.min.js"></script>
<script>
 $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    });

    $("#edit_absen").on('click',function(){
      var id_absen=$(this).attr("id_absen");
      var nama_lengkap=$(this).attr("data-nama");
      var nis=$(this).attr("data-nis");
      var absen=$(this).attr("data-absen");

      $("#id_absen").attr('value',id_absen);
      $("#nama_lengkap").html(nama_lengkap);
      $("#nis").html(nis);    
    });
</script>