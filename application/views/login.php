<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>asset/temp/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>asset/temp//bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>asset/temp//bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>asset/temp//dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>asset/temp//plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="login-box-body">
    <div class="text-center"><h4>Aplikasi Absen Siswa SDN 01 Maracang</h4></div>
    <br>
       <?= $this->session->flashdata('gagal'); ?>
    <br>
    <form action="<?php echo base_url();?>index.php/Login/login" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Email" name="username">
        <span class="fa fa-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="pass">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">        
        <!-- /.col -->
        <div class="pull-right" style="padding-right: 15px;">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Login <i class="fa fa-sign-in"></i></button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    <a href="#" role="menuitem" tabindex="-1" data-toggle="modal" data-target="#Lupa">Lupa Password ?</a>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<div class="modal fade" id="Lupa" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Kelas</h4>
        </div>
        <div class="modal-body">          
            <input type="text" id="id_kelas" value="" hidden="true" name="id_kelas">
            <p>Masukan NIP Anda Untuk Melihat Password.</p>
              <div class="form-group">               
                <input type="text" name="nip" class="form-control" value="" id="nip" required="true" placeholder="NIP">
              </div>
              <br>               
                  <p id="res"></p>
              <div class="row">
                <div class="pull-right" style="padding-right: 18px;">
                  <button class="btn btn-success" id="sub">Submit</button>                 
                </div>                
              </div>                            
          </div>
        </div>
      </div>
    </div>


<!-- jQuery 3 -->
<script src="<?php echo base_url();?>asset/temp/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>asset/temp/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>asset/temp/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });

$("#sub").on('click',function(){
  var nip=$("#nip").val();
  //alert(nip);
  $.ajax({  
    dataType : "JSON",   
    url  : '<?php echo base_url();?>index.php/Akun/getpass/'+nip
  }).done(function(data){
    for (var i = 0; i < data.length; i++) {
      //$("#nip").attr('value',data[i].password);
      var str="";
      str+="<b>Password Anda Adalah : "+data[i].password+"</b>";
      $("#res").html(str);
    }    
  });
});
</script>
</body>
</html>
