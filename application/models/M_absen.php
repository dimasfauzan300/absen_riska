<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_absen extends CI_Model {
	 function getdata(){
		date_default_timezone_set("Asia/Jakarta");
		$this->db->select("absen.nis as nis,siswa.nama_lengkap as nama_lengkap,tanggal,absen.absen as absen,absen.id_absen as id_absen");
		$this->db->from("absen");
		$this->db->join("siswa","absen.nis=siswa.nis");
		$this->db->where("tanggal",date("Y-m-d"));
		$res=$this->db->get();
		return $res->result_array();
	}


	 function insert_absen(){
		date_default_timezone_set("Asia/Jakarta");
		$absen=$this->input->post("absen");
		$nis=$this->input->post("nis");

		$tanggal=date("Y-m-d");

		// for ($i=0; $i < count($nis); $i++) { 
		// 	echo "NIS : ".$nis[$i]." Absen : ".$absen[$i];
		// 	echo "<br>";
		// }

		for ($i=0; $i < count($nis) ; $i++) {
			// echo "NIS : ".$nis[$i]."<br>Absen : ".$absen[$i]."<br>tanggal : ".date("Y-m-d");
			$this->db->where("absen.nis",$nis[$i]);
			$this->db->where("absen.tanggal",$tanggal);
			$res=$this->db->get("absen");
			$ceknis=0;
			if($ceknis==0){
				if($absen[$i]!=""){
					// $data = array('nis' => $nis[$i],
					// 	'absen' => $absen[$i],
					// 	'tanggal' =>$tanggal
					//  );
					$str="INSERT INTO `absen` (`id_absen`, `nis`, `absen`, `tanggal`) VALUES (NULL, '$nis[$i]', '$absen[$i]', '$tanggal')";
					$this->db->query($str);
					// $this->db->insert("absen",$data);
								
				}else{
					$data = array('nis' => $nis[$i],
						'absen' => "TK",
						'tanggal' =>$tanggal
					 );
					$this->db->insert("absen",$data);
					/*$str="INSERT INTO absen (id_absen,nis,absen,tanggal) VALUES (NULL,$nis[$i],TK,$tanggal)";
					$this->db->query($str);*/
					
				}
			}else if(count($nis)==1){
				
			}
		}
		redirect(base_url("index.php/Absen"));
	}

	 function edit($id,$data){
		$this->db->where('id_absen',$id);
		$this->db->update('absen',$data);
	}

	function get_data_print(){
		$this->db->select("absen.nis as nis,siswa.nama_lengkap as nama_lengkap,tanggal,absen.absen as absen,absen.id_absen as id_absen");
		$this->db->from("absen");
		$this->db->join("siswa","absen.nis=siswa.nis");
		$res=$this->db->get();
		return $res;
	}
}