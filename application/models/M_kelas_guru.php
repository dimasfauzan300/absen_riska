<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kelas_guru extends CI_Model {
	 function getsiswa($tahun_ajaran=""){		
		$this->db->select("semester.id_semester as id_semester,siswa.nis as nis,siswa.nama_lengkap as nama_lengkap,kelas.nama_kelas as nama_kelas,kelas_guru.id_kelas_guru as id_kelas_guru");
		$this->db->from("kelas_guru");
		$this->db->join("siswa","kelas_guru.nis=siswa.nis");
		$this->db->join("kelas","kelas_guru.id_kelas=kelas.id_kelas");
		$this->db->join("guru","kelas_guru.id_guru=guru.id_guru");
		$this->db->join("semester","kelas_guru.id_semester=semester.id_semester");
		$this->db->order_by("nis","asc");
		if($tahun_ajaran!=""){
			$this->db->where("semester.id_semester",$tahun_ajaran);
		}
		$res=$this->db->get();
		return $res->result_array();
	}

	 function insert_kelas($data){
		$this->db->insert("kelas",$data);
	}

	 function edit($id,$data){
		$this->db->where('id_kelas',$id);
		$this->db->update('kelas',$data);
	}

	function getsiswatambah(){
		$this->db->select("nis,nama_lengkap");
		$this->db->from("siswa");
		$this->db->order_by("nis","asc");
		$this->db->order_by("tahun_masuk","desc");
		$res=$this->db->get();
		return $res->result_array();
	}

	function simpan_kelas_siswa(){
		$id_kelas=$this->input->post("kelas");
		$siswa=$this->input->post("siswa");
		$id_semester=getSemesterAktif();
		$id_guru=$this->session->userdata('id_guru');
		// var_dump($siswa);exit;
		if(count($siswa)==NULL){
			$this->session->set_flashdata('gagal1', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Minimial 1 Siswa Harus Dipilih</div>");
			redirect(base_url("index.php/Kelas_guru/tambah_kelas_siswa"));
		}else if($id_kelas=="Pilih Kelas"||$id_kelas==""){
			$this->session->set_flashdata('gagal1', "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Alert!</h4> Pilih Kelas Terlebih Dahulu</div>");
			redirect(base_url("index.php/Kelas_guru/tambah_kelas_siswa"));
		}else{
			for($i=0;$i<count($siswa);$i++){
				/*echo "nis : ".$siswa[$i]." kelas : ".$id_kelas." semester : ".$id_semester." id guru : ".$id_guru;
				exit;*/			
				$str="INSERT INTO kelas_guru (id_kelas,id_guru,id_semester,nis) VALUES ($id_kelas,$id_guru,$id_semester,$siswa[$i])";
				$this->db->query($str);
			}
			$this->session->set_flashdata('gagal', "<div class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-check'></i> Sukses!</h4> Data Berhasil Disimpan</div>");
			redirect(base_url("index.php/Kelas_guru/"));
		}
		
	}

	function hapus($id_kelas_guru){
		$this->db->where("id_kelas_guru",$id_kelas_guru);
		$this->db->delete("kelas_guru");
		$this->session->set_flashdata('gagal', "<div class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-check'></i> Sukses!</h4> Data Berhasil Dihapus</div>");
		redirect(base_url("index.php/Kelas_guru/"));
	}

}