<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Tahun extends CI_Model {
	/* function getdata(){
		$this->db->select("*");
		$this->db->from("tahun_ajaran");
		$this->db->join("semester","tahun_ajaran.id_thn_ajaran=semester.id_thn_ajaran");		
		$res=$this->db->get();
		return $res->result_array();
	}*/

	 function getdata(){
		$this->db->order_by("id_semester","DESC");
		$res=$this->db->get("semester");
		return $res->result_array();
	}
	 function edit_semester($id,$data){
		$this->db->where('id_semester',$id);
		$this->db->update('semester',$data);
	}

	 function cek_aktif_semester(){
		$res=$this->db->get("semester");
		return $res->num_rows();
	}

	 function simpan_semester($data){
		$this->db->insert("semester",$data);
	}

	function gettaunaktif(){
		$this->db->select("id_semester");
		$this->db->from("semester");
		$this->db->where("periode_aktif",1);
		$res=$this->db->get();
		$smt="";
		foreach ($res->result() as $data) {
			$smt=$data->id_semester;
		}
		return $smt;
	}
}