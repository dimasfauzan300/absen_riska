<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kelas extends CI_Model {
	public function getdata(){		
		$res=$this->db->get('kelas');
		return $res->result_array();
	}

	public function insert_kelas($data){
		$nama_kelas=$data['nama_kelas'];
		$this->db->where("nama_kelas",$nama_kelas);
		$res=$this->db->get("kelas");
		if($res->num_rows()>0){
			$this->session->set_flashdata('gagal', "<div class='alert alert-warning alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <h4><i class='icon fa fa-warning'></i> Warning!</h4> Data Suadah Ada !</div>");
			redirect(base_url("index.php/Kelas"));
		}else{
			$this->db->insert("kelas",$data);
		}			
	}

	public function edit($id,$data){
		$this->db->where('id_kelas',$id);
		$this->db->update('kelas',$data);
	}

}