<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_akun extends CI_Model {
	function getdata(){		
		$res=$this->db->get('guru');
		return $res->result_array();
	}
	function simpan_akun($id_guru,$data){
		$this->db->where("id_guru",$id_guru);
		$this->db->update("guru",$data);
	}
	function getpassword($nip){
		$this->db->select("password");
		$this->db->from("guru");
		$this->db->where("guru.nip",$nip);
		$res=$this->db->get();
		return $res->result();
	}
}