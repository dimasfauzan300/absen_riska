<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_siswa extends CI_Model {
	public function getdata(){		
		$res=$this->db->get('siswa');
		return $res->result_array();
	}

	public function insert_siswa($data){
		$this->db->insert("siswa",$data);
	}

	public function edit($id,$data){
		$this->db->where('nis',$id);
		$this->db->update('siswa',$data);
	}
}