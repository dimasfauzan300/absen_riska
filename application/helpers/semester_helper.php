<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function getSemesterAktif(){
		$ci =& get_instance();
		$ci->load->database();
		$semester="";
		$ci->db->select("id_semester");
		$ci->db->from("semester");
		$ci->db->where("periode_aktif",1);
		$res=$ci->db->get();
		foreach ($res->result() as $data) {
			$semester=$data->id_semester;
		}
		return $semester;
	}

	function ceksiswa($nis){
		$ci =& get_instance();
		$ci->load->database();
		$stat="";
		$ci->db->select("nis");
		$ci->db->from("kelas_guru");
		$ci->db->where("kelas_guru.nis",$nis);
		$res=$ci->db->get();
		$cek=$res->num_rows();
		if($cek>0){
			return 1;			
		}else{
			return 0;
		}		
	}

	function cekabsen($nis){
		$ci =& get_instance();
		$ci->load->database();
		$stat="";
		$ci->db->select("nis");
		$ci->db->from("absen");
		$ci->db->where("absen.nis",$nis);
		$res=$ci->db->get();
		$cek=$res->num_rows();
		if($cek>0){
			return 1;			
		}else{
			return 0;
		}
	}